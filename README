Random Number Generator Library
-------------------------------

The goals of the Random Number Generator Library project are:

* Provide stable API/ABIs for applications and libraries to get random
  data for cryptographic and other purposes.  Simplicity is key, and
  for that reason we would like the API/ABI to be independent of other
  crypto interfaces.

* Universal cross-platform functionality.  Must work on at least GNU
  systems, BSD systems, Windows, and Mac OS X.  Must be suitable for
  embedded systems, with special handling of low-entropy situations.

* Provide a framework for pluging in different entropy generators
  without recompiling code.  Enable system administrators to configure
  which entropy provider should be used for applications via a
  configuration file.

* The framework and core library should be liberally licensed so that
  it is compatible with all reasonable free and open source licenses
  licenses.

* Designed and written with security in mind, following best
  implementation practices and tracking progress in the cryptographic
  research.

Initially the focus of the project is to provide a C library.
Bindings to other languages or compatible re-implementations in other
languages is within scope of the project.

Challenges
----------

A common cause for complexity (which leads to bugs), and portability
issues, of RNG implementations are system concepts like processes and
threads.  The fundamental issue is that two processes or threads must
get independent and random data every time it is needed.  There should
be no possibility for race conditions or other corner cases.  The code
needs to deal with process forks.  Unfortunately, solving this
requires access to system dependent functions that deals with
processes and threads.

Historically the random number interfaces provided by OS vendors have
had varying levels of quality, sometimes they have clearly been
unsuitable for cryptographic purposes.  Even when the interfaces are
well designed, environmental factors can lead to suboptimal results.
For example, embedded systems may not have any sources of entropy.
The OS is thus unable to produce good quality random numbers.

Background History
------------------

GnuTLS initially only worked with Werner Koch's Libgcrypt, so GnuTLS
naturally used libgcrypt's RNG functions.  Nikos Mavrogiannopoulos
enhanced GnuTLS to suport replaceable crypto backends, and part of
that work involved adding a crypto backend for Niels Möller's GNU
Nettle.  Unfortunately, Nettle does not provide any RNG functionality.
Code inspired from GNU LSH, to generate random numbers on top of GNU
Nettle, were added to GnuTLS as a consequence.

Other projects, including GNU SASL, were in the same situation as
GnuTLS.  GNU SASL wanted to support both Libgcrypt and GNU Nettle as
the crypto backend.  Copying the (non-trivial) code from GnuTLS to GNU
SASL to deal with the random number generation on top of GNU Nettle
would create a maintenance problem.

Realizing this issue, Simon Josefsson set out the goals of the Librng
project and worked on assembling the starting point for a new library.
