#ifndef RNG_H
# define RNG_H

# ifdef __cplusplus
extern "C"
{
# endif

# include <stddef.h>		/* size_t */

  /**
   * RNG_VERSION
   *
   * Pre-processor symbol with a string that describe the header file
   * version number.  Used together with rng_check_version() to verify
   * header file and run-time library consistency.
   */
#define RNG_VERSION "0.0.0"

  /**
   * RNG_VERSION_NUMBER
   *
   * Pre-processor symbol with a hexadecimal value describing the header
   * file version number.  For example, when the header version is 1.2.3
   * this symbol will have the value 0x01020300.  The last two digits
   * are only used between public releases, and will otherwise be 00.
   */
#define RNG_VERSION_NUMBER 0x00000000

  /**
   * rng_rc:
   * @RNG_OK: Successful return code, guaranteed to be always 0.
   * @RNG_FILE_ERROR: System error related to reading files.
   *
   * Error codes for library functions.
   */
  typedef enum
    {
      RNG_OK = 0,
      RNG_FILE_ERROR = -1,
    } rng_rc;

  /**
   * rng_ctx
   *
   * Library handle to hold internal state of librng.
   */
  typedef struct _rng_ctx *rng_ctx;

  /* Library initialization/deinitialization. */
  extern int rng_init (rng_ctx *ctx, int flags);
  extern void rng_done (rng_ctx ctx);
  extern const char *rng_check_version (const char *req_version);

  /* Error handling. */
  extern const char *rng_strerror (int err);
  extern const char *rng_strerror_name (int err);

  /* Mutex handling. */
  typedef int (*rng_mutex_init_func) (void **mutex);
  typedef int (*rng_mutex_lock_func) (void **mutex);
  typedef int (*rng_mutex_unlock_func) (void **mutex);
  typedef int (*rng_mutex_deinit_func) (void **mutex);

  extern void rng_set_mutex (rng_ctx *ctx, rng_mutex_init_func init,
			     rng_mutex_deinit_func deinit,
			     rng_mutex_lock_func lock,
			     rng_mutex_unlock_func unlock);

  /* Core functions. */
  extern int rng_nonce (rng_ctx * ctx, char *data, size_t datalen);
  extern int rng_pseudo_random (rng_ctx * ctx, char *data, size_t datalen);
  extern int rng_slow_random (rng_ctx * ctx, char *data, size_t datalen);

  /* Query interface. */
  extern int rng_generator_get (rng_ctx * ctx, const char **name);
  extern int rng_generator_set (rng_ctx * ctx, const char *name);

# ifdef __cplusplus
}
# endif

#endif /* RNG_H */
